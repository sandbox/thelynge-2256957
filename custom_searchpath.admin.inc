<?php

/**
 * @file
 * Hooks to setup a configuration page.
 */

/**
 * Setup the form on the configuration page.
 */
function custom_searchpath_admin_settings() {
  $form = array();

  $form['custom_searchpath_path'] = array(
    '#type' => 'textfield',
    '#title' => 'Custom searchpath',
    '#default_value' => variable_get('custom_searchpath_path', ''),
  );

  return system_settings_form($form);
}
