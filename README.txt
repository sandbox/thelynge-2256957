INTRODUCTION
------------
This module lets you set a custom searchpath to replace the default 'search/node/*'.
Notice: If you have Apache Solr activated, you still to keep "Node" activated in the Search settings -> Active search modules.

INSTALLATION
------------
Install as you would normally install a contributed drupal module.

CONFIGURATION
-------------
Configure the module on the configuration page: 
Home » Administration » Configuration » Search and metadata » Custom searchpath
(admin/config/search/custom_searchpath)

MAINTAINERS
-----------
* thelynge - https://drupal.org/user/2703525
